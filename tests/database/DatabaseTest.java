package database;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseTest {
    Database database = new Database("jdbc:mysql://localhost/информация о студентах?serverTimezone=Europe/Moscow&useSSL=false", "root", null);

    @Test
    public void testCountColumn() {
        try {
            database.connect();

            int numColumns = database.countColumns("паспортные данные студента");
            Assert.assertEquals(14, numColumns);

            numColumns = database.countColumns("свидетельство о рождении");
            Assert.assertEquals(6, numColumns);

            numColumns = database.countColumns("сведения о родителях");
            Assert.assertEquals(10, numColumns);

            numColumns = database.countColumns("сведения о студентах");
            Assert.assertEquals(9, numColumns);

            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReceivingColumns() {
        try {
            database.connect();

            String table = "свидетельство о рождении";

            ArrayList<String> columnNames = database.getColumnNames(table, database.countColumns(table), 1);

            ArrayList<String> columnNames1 = new ArrayList<>();
            columnNames1.add("id");
            columnNames1.add("Код студента");
            columnNames1.add("Серия");
            columnNames1.add("Номер");
            columnNames1.add("Кем выдан");
            columnNames1.add("Дата выдачи");

            if (columnNames.equals(columnNames1))
                Assert.assertTrue(true);
            else
                Assert.fail();

            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetTableNames() {
        try {
            database.connect();

            ArrayList<String> tableNames = database.getTableNames();

            ArrayList<String> tableNames1 = new ArrayList<>();
            tableNames1.add("паспортные данные студента");
            tableNames1.add("свидетельство о рождении");
            tableNames1.add("сведения о родителях");
            tableNames1.add("сведения о студентах");

            if (tableNames.equals(tableNames1))
                Assert.assertTrue(true);
            else
                Assert.fail();

            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConnection() {
        try {
            Assert.assertTrue(database.connect());
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdateRecord() {
        try {
            database.connect();
            String sql = "UPDATE `сведения о студентах` SET `Город`='Москва' WHERE id='3';";
            Assert.assertEquals(1, database.requestUpdate(sql));
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRemoveRecord() {
        try {
            database.connect();
            String sql = "DELETE FROM `сведения о студентах` WHERE id='3';";
            Assert.assertEquals(1, database.requestUpdate(sql));
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddRecord() {
        try {
            database.connect();
            String sql = "INSERT INTO `сведения о студентах` VALUES ('3', '32', 'Макар Матвей Петрович', '2001-04-02', 'Пенза', 'Гагарина', '21', '3', '90921412114');";;
            Assert.assertEquals(1, database.requestUpdate(sql));
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}