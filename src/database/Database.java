package database;

import java.sql.*;
import java.util.ArrayList;

public class Database {
    private String url;
    private String user;
    private String password;

    private Connection con;
    private Statement stmt;


    /**
     * @param url - путь к базе данных
     * @param user - имя пользователя
     * @param password - пароль
     */
    public Database(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    /**
     * Подключение к базе данных
     *
     * @return - результат подключения
     * @throws SQLException - ошибка с SQL кодом при подключении к базе данных
     */
    public boolean connect() throws SQLException {
        con = DriverManager.getConnection(url, user, password);
        stmt = con.createStatement();

        return true;
    }

    /**
     * Отключение от базы данных
     *
     * @throws SQLException - ошибка с SQL кодом при отклучении от базы данных
     */
    public void close() throws SQLException {
        con.close();
        stmt.close();
    }

    /**
     * Метод выполняет обычный запрос SQL без возможности изменять данные
     *
     * @param sql - запрос SQL
     * @return - результат запроса SQL
     * @throws SQLException - ошибка с SQL кодом
     */
    public ResultSet request(String sql) throws SQLException {
        return stmt.executeQuery(sql);
    }

    /**
     * Метод выполняет запрос SQL c возможностью изменять данные
     *
     * @param sql - запрос SQL
     * @return - результат запроса SQL
     * @throws SQLException - ошибка с SQL кодом
     */
    public int requestUpdate(String sql) throws SQLException {
        return stmt.executeUpdate(sql);
    }

    /**
     * Метод подсчитывает количество колонок в конкретной таблице
     *
     * @param tableName - имя таблицы
     * @return - результат запроса
     * @throws SQLException - ошибка с SQL кодом
     */
    public int countColumns(String tableName) throws SQLException {
        return request("SELECT * FROM `" + tableName + "`;").getMetaData().getColumnCount();
    }

    /**
     * Метод создаёт список с названиями клонок конкретной таблицы
     *
     * @param tableName - имя таблицы
     * @param numberColumns - кол-во колонок
     * @param startColumn - номер колонки с которой следует начать
     * @return - список с названиями колонок
     * @throws SQLException - ошибка с SQL кодом
     */
    public ArrayList<String> getColumnNames(String tableName, int numberColumns, int startColumn) throws SQLException {
        ArrayList<String> columnNames = new ArrayList<>();

        ResultSetMetaData rsm = request("SELECT * FROM `" + tableName + "`;").getMetaData();
        for (int i = startColumn; i <= numberColumns; i++) {
            columnNames.add(rsm.getColumnName(i));
        }

        return columnNames;
    }

    /**
     * Метод создаёт список с названиями таблиц в базе данных
     *
     * @return - список с именами таблиц
     * @throws SQLException - ошибка с SQL кодом
     */
    public ArrayList<String> getTableNames() throws SQLException {
        ArrayList<String> tableNames = new ArrayList<>();

        ResultSet rs = request("SHOW TABLES;");
        while (rs.next()) {
            tableNames.add(rs.getString(1));
        }
        rs.close();

        return tableNames;
    }
}
