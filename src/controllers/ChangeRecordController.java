package controllers;

import database.Database;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ChangeRecordController {

    @FXML
    private TextField numRecord;

    @FXML
    private Button selectBtn;

    @FXML
    private VBox vBox;

    private DatabaseEditorController databaseEditorController;

    private Database dataBase;

    private ArrayList<TextField> textFields;
    private ArrayList<String> columnNames;

    public ChangeRecordController(DatabaseEditorController databaseEditorController) {
        this.databaseEditorController = databaseEditorController;
    }

    /**
     * Инитиализирует исходные данные для корректной работы программы
     */
    public void initialize() throws SQLException {
        dataBase = databaseEditorController.getDatabase();
        textFields = new ArrayList<>();
        Button changeBtn = new Button("Изменить");
        changeBtn.setFont(Font.font(17));

        generateForm();

        VBox vbox2 = new VBox();
        vbox2.setPrefHeight(50);
        vBox.getChildren().addAll(vbox2, changeBtn);

        selectBtn.setOnAction(actionEvent -> {
            try {
                addValueTextField(Integer.parseInt(numRecord.getText()));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        changeBtn.setOnAction(actionEvent -> {
            try {
                changeRow(databaseEditorController.getCurrentTable(), columnNames, getValues(), Integer.parseInt(numRecord.getText()));
                databaseEditorController.showTableData(databaseEditorController.getCurrentTable());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Создаёт SQL запросы на изменение данных в базе данных конкретной таблицы
     *
     * @param tableName - имя таблицы
     * @param columnName - список с именами колонок
     * @param value - список с значениями
     * @param numRow - номер записи
     * @throws SQLException - ошибка с SQL кодом
     */
    public void changeRow(String tableName, ArrayList<String> columnName, ArrayList<String> value, int numRow) throws SQLException {
        for (int i = 1; i < columnName.size(); i++) {
            dataBase.requestUpdate("UPDATE `" + tableName + "` SET `" + columnName.get(i) + "`='" + value.get(i) + "' WHERE id='" + numRow + "';");
        }

        dataBase.requestUpdate("UPDATE `" + tableName + "` SET `" + columnName.get(0) + "`='" + value.get(0) + "' WHERE id='" + numRow + "';");
    }

    /**
     * Генерирует форму для ввода данных
     *
     * @throws SQLException - ошибка с SQL кодом
     */
    private void generateForm() throws SQLException {
        String currentTable = databaseEditorController.getCurrentTable();
        columnNames = dataBase.getColumnNames(currentTable, dataBase.countColumns(currentTable), 1);
        ArrayList<Label> labels = createLabels(columnNames);
        createTextFields(columnNames.size());

        for (int i = 0; i < columnNames.size(); i++) {
            vBox.getChildren().add(labels.get(i));
            vBox.getChildren().add(textFields.get(i));
        }
    }

    /**
     * Создаёт список с метками (Label)
     *
     * @param columnNames - список с именами колонок
     * @return - список с метками (Label)
     */
    private ArrayList<Label> createLabels(ArrayList<String> columnNames) {
        ArrayList<Label> labels = new ArrayList<>();
        for (String columnName : columnNames) {
            Label label = new Label();
            label.setText(columnName);
            label.setTextAlignment(TextAlignment.LEFT);
            label.setFont(Font.font(17));
            labels.add(label);
        }

        return labels;
    }

    /**
     * Создаёт список с полями для ввода данных
     *
     * @param columnsCount - кол-во колонок
     */
    private void createTextFields(int columnsCount) {
        for (int i = 0; i < columnsCount; i++) {
            TextField textField = new TextField();
            textField.setAlignment(Pos.CENTER);
            textFields.add(textField);
        }
    }

    /**
     * Получает значения полей для ввода данных
     *
     * @return - список значений
     */
    private ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();

        for (TextField textField : textFields) {
            values.add(textField.getText());
        }

        return values;
    }

    /**
     * Отображает значения в полях для ввода данных
     *
     * @param numRecord - номер записи
     * @throws SQLException - ошибка с SQL кодом
     */
    private void addValueTextField(int numRecord) throws SQLException {
        ResultSet rs = dataBase.request("SELECT * FROM `" + databaseEditorController.getCurrentTable() + "`");
        while (rs.next()) {
            if (rs.getInt(1) == numRecord) {
                for (int i = 1; i <= textFields.size(); i++) {
                    textFields.get(i - 1).setText(rs.getString(i));
                }
            }
        }
        rs.close();
    }
}
