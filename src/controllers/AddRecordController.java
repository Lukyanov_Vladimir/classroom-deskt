package controllers;
import database.Database;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddRecordController {

    @FXML
    private VBox vBox;

    private ArrayList<TextField> textFields;

    private DatabaseEditorController databaseEditorController;

    private Database dataBase;
    private Button addBtn;

    public AddRecordController(DatabaseEditorController databaseEditorController) {
        this.databaseEditorController = databaseEditorController;
    }

    /**
     * Инитиализирует исходные данные для корректной работы программы
     */
    public void initialize() {
        textFields = new ArrayList<>();
        addBtn = new Button("Добавить");
        addBtn.setFont(Font.font(17));

        dataBase = databaseEditorController.getDatabase();

        try {
            generateForm();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        addBtn.setOnAction(actionEvent -> {
            try {
                addRow(databaseEditorController.getCurrentTable(), getValues());
                databaseEditorController.showTableData(databaseEditorController.getCurrentTable());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Генерирует форму для ввода данных
     *
     * @throws SQLException - ошибка с SQL кодом
     */
    private void generateForm() throws SQLException {
        String currentTable = databaseEditorController.getCurrentTable();
        ArrayList<String> columnNames = dataBase.getColumnNames(currentTable, dataBase.countColumns(currentTable), 1);
        ArrayList<Label> labels = createLabels(columnNames);
        createTextFields(columnNames.size());

        for (int i = 0; i < columnNames.size(); i++) {
            vBox.getChildren().add(labels.get(i));
            vBox.getChildren().add(textFields.get(i));
        }

        VBox vbox2 = new VBox();
        vbox2.setPrefHeight(30);
        vBox.getChildren().addAll(vbox2, addBtn);
    }

    /**
     * Создаёт список с метками (Label)
     *
     * @param columnNames - список с именами колонок
     * @return - список с метками (Label)
     */
    private ArrayList<Label> createLabels(ArrayList<String> columnNames) {
        ArrayList<Label> labels = new ArrayList<>();
        for (String columnName : columnNames) {
            Label label = new Label();
            label.setText(columnName);
            label.setTextAlignment(TextAlignment.LEFT);
            label.setFont(Font.font(17));
            labels.add(label);
        }

        return labels;
    }

    /**
     * Создаёт список с полями для ввода данных
     *
     * @param columnsCount - кол-во колонок
     */
    private void createTextFields(int columnsCount) {
        for (int i = 0; i < columnsCount; i++) {
            TextField textField = new TextField();
            textField.setAlignment(Pos.CENTER);
            textFields.add(textField);
        }
    }

    /**
     * Получает значения полей для ввода данных
     *
     * @return - список значений
     */
    private ArrayList<String> getValues() {
        ArrayList<String> values = new ArrayList<>();

        for (TextField textField : textFields) {
            values.add(textField.getText());
        }

        return values;
    }

    /**
     * Создаёт SQL запрос на добавление записи в базу данных конкретной таблицы
     *
     * @param tableName - имя таблицы
     * @param values - список значений
     * @throws SQLException - ошибка с SQL кодом
     */
    public void addRow(String tableName, ArrayList<String> values) throws SQLException {
        ResultSetMetaData rsm = dataBase.request("SELECT * FROM `" + tableName + "`").getMetaData();
        ArrayList<String> columnNames = dataBase.getColumnNames(tableName, rsm.getColumnCount(), 1);

        String query = "INSERT INTO `" + tableName + "` (" +
                genColumsNames(columnNames) + ") VALUES (" +
                genValues(values) + ");";
        dataBase.requestUpdate(query);
    }

    /**
     * Метод генерирует строку с именами колонок для SQL запроса
     *
     * @param columnNames - список с именами колонок
     * @return - строку с именами колонок
     */
    private String genColumsNames(ArrayList<String> columnNames) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < columnNames.size(); i++) {
            if (i != columnNames.size() - 1) {
                stringBuilder.append("`").append(columnNames.get(i)).append("`, ");

            } else {
                stringBuilder.append("`").append(columnNames.get(i)).append("`");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Метод генерирует строку с значениями для SQL запроса
     *
     * @param values - список с значениями
     * @return - строку с значениями
     */
    private String genValues(ArrayList<String> values) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {

            if (i != values.size() - 1) {
                stringBuilder.append("'").append(values.get(i)).append("', ");

            } else {
                stringBuilder.append("'").append(values.get(i)).append("'");
            }
        }

        return stringBuilder.toString();
    }
}
