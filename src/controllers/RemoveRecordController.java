package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.SQLException;

public class RemoveRecordController {

    @FXML
    private TextField numId;

    @FXML
    private Button removeBtn;

    private DatabaseEditorController databaseEditorController;

    public void setDatabaseEditorController(DatabaseEditorController databaseEditorController) {
        this.databaseEditorController = databaseEditorController;
    }

    /**
     * Инитиализирует исходные данные для корректной работы программы
     */
    @FXML
    public void initialize() {
        removeBtn.setOnAction(event -> {
            try {
                String currentTable = databaseEditorController.getCurrentTable();
                removeRecord(currentTable, Integer.parseInt(numId.getText()));
                databaseEditorController.showTableData(currentTable);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Создаёт запрос на удалние записи из базы данных конкретной таблицы
     *
     * @param tableName - имя таблицы
     * @param numId - номер записи
     * @throws SQLException - ошибка с SQL кодом
     */
    private void removeRecord(String tableName, int numId) throws SQLException {
        databaseEditorController.getDatabase().requestUpdate("DELETE FROM `" + tableName + "` WHERE id='" + numId + "';");
    }
}