import controllers.DatabaseEditorController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/databaseEditor.fxml"));
        Parent root = fxmlLoader.load();
        primaryStage.setTitle("Настольное приложение классного руководителя");
        primaryStage.centerOnScreen();
        primaryStage.setResizable(true);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        DatabaseEditorController dec = fxmlLoader.getController();
        dec.setStage(primaryStage);
        primaryStage.setOnCloseRequest(dec.getCrossClosing());
    }
}
